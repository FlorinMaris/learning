# Web Shop Web App

This is a basic implementation of the client side of a web shop.

It offers the shop managers functionalities like:
*browsing products 
*adding products to chart
*adding comments to bought products
*ordering products



The project will uses the following 

* [Maven](https://maven.apache.org/)
* [Spring boot](https://spring.io/guides/gs/serving-web-content/)
* [Tymeleaf](http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html)
* [Webjars](https://www.webjars.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [Docker](https://www.docker.com/)
* [PostgreSQL](https://www.postgresql.org/)

