package org.flo.shop;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

    @RequestMapping("/")
    public String welcome() {

        return "home";
    }

    @RequestMapping("/sign_in")
    public String mock() {

        return "sign_in";
    }

    @RequestMapping("/register")
    public String register() {

        return "register";
    }

    @RequestMapping("/help")
    public String help() {

        return "help";
    }

    @RequestMapping("/product1")
    public String product1(Map<String, Object> model) {

        return "product1";
    }

    @RequestMapping("/search")
    public String search() {
        return "search";
    }

    @RequestMapping("/shopping_cart")
    public String shoppingCart() {
        return "shopping_cart";
    }

    @RequestMapping("/in_development")
    public String underConstruction() {

        return "in_development";
    }

}