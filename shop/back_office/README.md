# Back Office Web App

This is a basic implementation of a back office side of a web shop.

It offers the shop managers functionalities like:
*adding removing of products
*updating product details
*adding users 
*managing clients user details
*managing product comments
   
 
This project is a starting point for a local maven archetype.

The project will use the following 

* [Maven](https://maven.apache.org/)
* [Spring boot](https://spring.io/guides/gs/serving-web-content/)
* [Tymeleaf](http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html)
* [Webjars](https://www.webjars.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [Docker](https://www.docker.com/)
* [PostgreSQL](https://www.postgresql.org/)

