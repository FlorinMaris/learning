package org.flo.shop;


import org.flo.shop.entities.Comment;
import org.flo.shop.entities.Product;
import org.flo.shop.entities.ProductModel;
import org.flo.shop.repository.CommentRepository;
import org.flo.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Controller
public class ProductsController {

    @Value("${application.image.storage.folder}")
    String imagePath;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    CommentRepository commentRepository;

    @RequestMapping("/add_product_view")
    public String showProducts(Model model) {
        model.addAttribute("product", new ProductModel());
        return "add_product";
    }

    @PostMapping("/add_product")
    public String addProduct(@ModelAttribute ProductModel productModel, Model model
    ) {
        Product product = new Product();
        product.updateFields(productModel.getName(),
                productModel.getQuantity(),
                productModel.getPrice(),
                productModel.getDescription(),
                imagePath,
                productModel.getUnit());
        productRepository.save(product);
        product.setImagePath();
        writeImageFile(product, productModel.getImage());
        productRepository.save(product);
        model.addAttribute("product", product);
        return "preview_product";
    }

    @RequestMapping("/modify_product_view")
    public String modifyProductView(@RequestParam(value="productId") Long productId, Model model
    ) {
        Product product =productRepository.findOne(productId);
        ProductModel productModel = new ProductModel();
        productModel.setId(product.getId());
        productModel.setName(product.getName());
        productModel.setQuantity(product.getQuantity());
        productModel.setUnit(product.getUnit());
        productModel.setPrice(product.getPrice());
        productModel.setDescription(product.getDescription());
        model.addAttribute("product", productModel);
        return "modify_product";
    }

    @PostMapping("/modify_product")
    public String modifyProduct(@ModelAttribute ProductModel productModel, Model model) {

        Product product = productRepository.findOne(productModel.getId());
        product.updateFields(productModel.getName(),
                productModel.getQuantity(),
                productModel.getPrice(),
                productModel.getDescription(),
                imagePath,
                productModel.getUnit());
        if (!productModel.getImage().isEmpty()) {
            deleteImageFile(product);
            writeImageFile(product, productModel.getImage());
        }
        productRepository.save(product);
        model.addAttribute("product", product);
        return "preview_product";
    }

    @RequestMapping("/delete_product")
    public String deleteProduct(@RequestParam(value = "productId") Long productId, Model model) {
        Product product=productRepository.findOne(productId);
        deleteImageFile(product);
        commentRepository.delete(product.getComments());
        productRepository.delete(product.getId());
        Iterable<Product> productList = productRepository.findAll();
        model.addAttribute("products", productList);
        return "products";
    }
    @RequestMapping("/manage_product_comments")
    public String manageComments(@RequestParam(value="productId") Long productId, Model model){
        Product product=productRepository.findOne(productId);
        model.addAttribute("product",product);
        return "product_comments";
    }

    public void writeImageFile(Product product, MultipartFile file) {
        File imageFile = new File(imagePath + product.getImagePath());
        try {
            file.transferTo(imageFile);
        } catch (Exception e) {
            System.out.println("File could not be saved");
        }
    }

    public void deleteImageFile(Product product) {
        Path fileToDelete = Paths.get(this.imagePath + product.getImagePath());
        try {
            Files.deleteIfExists(fileToDelete);
        } catch (DirectoryNotEmptyException notEmptyException) {
            System.out.printf("Directory is not empty");
            notEmptyException.printStackTrace();
        } catch (IOException io) {
            System.out.println("IO exception");
            io.printStackTrace();
        } catch (SecurityException securityException) {
            System.out.println("Security exception");
            securityException.printStackTrace();
        }
    }

   }
