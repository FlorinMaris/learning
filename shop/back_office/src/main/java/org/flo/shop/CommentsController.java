package org.flo.shop;

import org.flo.shop.entities.Comment;
import org.flo.shop.entities.Product;
import org.flo.shop.repository.CommentRepository;
import org.flo.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
public class CommentsController {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    ProductRepository productRepository;

    @RequestMapping("/add_comment")
    public String addComment(
            @RequestParam(value = "productId") Long productId,
            @RequestParam(value = "commentTitle") String commentTitle,
            @RequestParam(value = "commentText") String commentText,
            Model model) {
        Product product = productRepository.findOne(productId);
        Comment comment = new Comment();
        comment.setProduct(product);
        comment.setTitle(commentTitle);
        comment.setText(commentText);
        commentRepository.save(comment);
        product = productRepository.findOne(productId);
        model.addAttribute("product", product);
        return "product_comments";
    }
    @RequestMapping("/disapprove_comment")
    public String disapproveComment(
            @RequestParam(value="productId") Long productId,
            @RequestParam(value="commentId") Long commentId,
            Model model){
        disapproveComment(commentId);
        model.addAttribute("product", productRepository.findOne(productId));
        return "product_comments";
    }
    @RequestMapping("/approve_comment")
    public String approveComment(
            @RequestParam(value="productId") Long productId,
            @RequestParam(value="commentId") Long commentId,
            Model model){
        approveComment(commentId);
        model.addAttribute("product", productRepository.findOne(productId));
        return "product_comments";
    }
    @RequestMapping("/delete_comment")
    public String deleteComment(
            @RequestParam(value="productId") Long productId,
            @RequestParam(value="commentId") Long commentId,
            Model model){
        commentRepository.delete(commentId);
        model.addAttribute("product", productRepository.findOne(productId));
        return "product_comments";
    }
    @RequestMapping("/approve_multiple_comments")
    public String approveMultipleComments(
            @RequestParam(value = "productId") Long productId,
            @RequestParam(value = "approvedCommentIds") List<Long> approvedCommentIds,
            Model model){
        Product product=productRepository.findOne(productId);
        for (Long commentId: approvedCommentIds) {
            approveComment(commentId);
        }
            List<Product> productList = new ArrayList<>();
            model.addAttribute("products", productList);
            return "unapproved_comments";

    }

    public void approveComment(Long commentId){
        Comment comment=commentRepository.findOne(commentId);
        comment.setApproved(true);
        commentRepository.save(comment);
    }
    public void disapproveComment(Long commentId){
        Comment comment=commentRepository.findOne(commentId);
        comment.setApproved(false);
        commentRepository.save(comment);
    }
    public void getProductsWithUnapprovedComments(List productList){

        for (Product comment : productRepository ) {

        }
    }

}
