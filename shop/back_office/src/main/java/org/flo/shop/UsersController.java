package org.flo.shop;

import org.flo.shop.entities.User;
import org.flo.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class UsersController {
    @Autowired
    UserRepository userRepository;

    @RequestMapping("/add_user_view")
    public String addUserView(Model model){
        model.addAttribute("user", new User());
        return "add_user";
    }
    @PostMapping("/add_user")
    public String addUser(@ModelAttribute User user, Model model){
        userRepository.save(user);
        Iterable<User> userList=userRepository.findAll();
        model.addAttribute("users",userList);
        return "users";
    }

}
