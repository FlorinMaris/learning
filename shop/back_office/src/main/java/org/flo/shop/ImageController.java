package org.flo.shop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

@RestController
public class ImageController {

    @Value("${application.image.storage.folder}")
    String imagePath;

    @PostConstruct
    private void initializeImageRepository() throws Exception {
        File imagesFolder = new File(imagePath);
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }
        if (imagesFolder.isFile()) {
            throw new Exception(imagePath + " already exists and is a file");
        }
    }

    @RequestMapping(value = "/images/{imageName:.+}"/*the .+ sign is here to enable us to get the extension of the file */, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("imageName") String imageName) throws Exception {
        byte[] imageContent = new byte[]{};// read from file
        File imageFile = new File(imagePath +"/"+ imageName);
        if (imageFile.isFile()) {
            imageContent = Files.readAllBytes(imageFile.toPath());
        }
        return ResponseEntity.ok(imageContent);
    }

}
