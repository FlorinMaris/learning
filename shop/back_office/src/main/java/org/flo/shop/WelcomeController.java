package org.flo.shop;


import org.flo.shop.entities.Product;
import org.flo.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


@Controller
public class WelcomeController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping("/")
    public String welcome() {

        return "home";
    }

    @RequestMapping("/sign_in")
    public String mock() {

        return "sign_in";
    }
    @RequestMapping("/register")
    public String register() {

        return "register";
    }
    @RequestMapping("/help")
    public String help() {

        return "help";
    }

    @RequestMapping("/search")
    public String search(){

        return "search";
    }

    @RequestMapping("/in_development")
    public String underConstruction() {

        return "in_development";
    }

    @RequestMapping("/view_users")
    public String showUsers() {

        return "users";
    }

    @RequestMapping("/view_products")
    public String showProducts(Model model) {
        Iterable<Product> productList = productRepository.findAll();
        model.addAttribute("products", productList);
        return "products";
    }
    @RequestMapping("/unapproved_comments")
    public String showUnapprovedComments(Model model) {
        Iterable<Product> productList = productRepository.findAll();
        model.addAttribute("products", productList);
        return "unapproved_comments";
    }
}