package org.flo.shop;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackOfficeApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BackOfficeApplication.class, args);
	}

}