package org.flo.shop.entities;

public enum ProductUnit {
    pc, litre, kilogram, set, pair;
}
