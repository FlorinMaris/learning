package org.flo.shop.repository;

import org.flo.shop.entities.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order,Long>{
}
