package org.flo.shop.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orders")
public class Order implements Serializable{
    @Id
    @GeneratedValue
    private long id;
    @OneToOne
    private User user;
    public enum Status {OrderRecieved, Sent, Completed}
    @Enumerated
    private Status status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
