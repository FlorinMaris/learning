package org.flo.shop.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Entity
public class Product implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String name;
    @Column
    private int quantity;
    @Column
    private double price;
    @Column
    private String description;
    @Column
    private String imagePath;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "product")
    private Collection<Comment> comments;

    @Enumerated
    private ProductUnit unit;

    public Product() {
    }

    public void updateFields(String name, int quantity, double price, String description, String imagePath, ProductUnit unit) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.description = description;
        this.unit = unit;
    }

    public ProductUnit getUnit() {
        return unit;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath() {
        this.imagePath = "/" + this.id + ".jpg";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUnit(ProductUnit unit) {
        this.unit = unit;
    }

    public long getId() {
        return id;
    }

    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}

