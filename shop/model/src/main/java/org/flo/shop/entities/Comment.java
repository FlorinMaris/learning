package org.flo.shop.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Comment implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    @Column(length = 600)
    private String text;
    @Column
    private String title;
    @Column
    private boolean approved;
    @OneToOne
    private User user;
    @OneToOne
    private Product product;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean hidden) {
        this.approved = hidden;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
