package org.flo.shop.repository;

import org.flo.shop.entities.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment,Long>{
}
