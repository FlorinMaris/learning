# Url checker

A basic web app, that uses threads to check if a set of URLs are up, 
the user adds a desired Url that will be checked every 2 seconds...
the page refreshes every 20 seconds to display a status of all the loaded URLs

A running version of the app can be viewed [here](https://akira.ddns.net/url-checker/) 


The project will uses the following 

* [Maven](https://maven.apache.org/)
* [Spring boot](https://spring.io/guides/gs/serving-web-content/)
* [Tymeleaf](http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html)
* [Webjars](https://www.webjars.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)



