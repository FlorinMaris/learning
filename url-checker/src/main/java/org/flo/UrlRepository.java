package org.flo;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;

@Component
public class UrlRepository {
    private HashSet<Url> urls = new HashSet<>();

    public void add(String urlAddress) {
        Url url = new Url();
        url.setAddress(urlAddress);
        urls.add(url);
    }

    public Collection<Url> getUrls() {
        return urls;
    }

    @Scheduled(fixedRate = 2000)
    void run() {

        for (Url url : urls) {
            new Thread(new Task(url)).start();
        }
    }

    static class Task implements Runnable {
        private final Url url;

        public Task(Url url) {
            this.url = url;
        }

        @Override
        public void run() {
            Date now = new Date();
            url.setLastChecked(now);
            url.setThreadName(Thread.currentThread().getName());
            try{
                HttpClientBuilder builder = HttpClientBuilder.create();
                CloseableHttpClient client = builder.build();
                HttpGet httpGet = new HttpGet(url.getAddress());
                final CloseableHttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode()== HttpStatus.SC_OK){
                    url.setState(Url.States.UP);
                }else {
                    url.setState(Url.States.DOWN);
                }
            }catch (Exception e){
                url.setState(Url.States.DOWN);
            }

        }
    }
}
