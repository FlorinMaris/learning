package org.flo;

import java.util.Date;

public class Url {
   private String address;
   public enum States{ UP,DOWN }
   private States state;
   private Date lastChecked;
   private String threadName;

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        address=addHttpToUrlIfMissing(address);
        this.address = address;
    }

    private String addHttpToUrlIfMissing(String urlAddress){
        if (!urlAddress.contains("http://")){
            return "http://"+urlAddress;
        }
        return urlAddress;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public Date getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(Date lastChecked) {
        this.lastChecked = lastChecked;
    }
}
