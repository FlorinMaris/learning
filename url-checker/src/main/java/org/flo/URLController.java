package org.flo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class URLController {

    @Autowired
    UrlRepository urlRepository;

    @RequestMapping("/")
    public String welcome(Model model) {
        model.addAttribute("urlList", urlRepository.getUrls());
        return "welcome";
    }

    @RequestMapping("/add_url")
    public String addUrl(@RequestParam(value = "urlAddress") String urlAddress, Model model) {
        urlRepository.add(urlAddress);
        model.addAttribute("urlList", urlRepository.getUrls());
        return "redirect:/";
    }

}