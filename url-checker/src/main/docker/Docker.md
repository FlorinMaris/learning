# Docker
We use the spotify docker maven plugin to build the image. 
[More info can be found here](https://github.com/spotify/docker-maven-plugin)

## How to build

```bash
mvn clean package docker:build
```

## How to run 
```bash
docker run \
--name url-checker \
--publish=38080:8080 \
--restart=always \
-d org.flo/url-checker

```