package org.flo;

import java.time.LocalDateTime;

public class Url implements Comparable<Url> {
    private String target;
    private String shortUrl;
    private LocalDateTime expiresOn;

    @Override
    public int compareTo(Url url) {
        return expiresOn.compareTo(url.expiresOn);
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        if (target.startsWith("http")) {
        this.target = target;
        } else  {
         this.target="http://"+target;
        }
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public LocalDateTime getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(LocalDateTime expiresOn) {
        this.expiresOn = expiresOn;
    }
}
