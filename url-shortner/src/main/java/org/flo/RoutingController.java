package org.flo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpHeaders.LOCATION;

@RestController
public class RoutingController {

    @Autowired
    UrlRepository urlRepository;

    @RequestMapping("/{shortUrl}")
    public ResponseEntity<String> redirectUrl(@PathVariable("shortUrl") String shortUrl) {
        Url url = urlRepository.getUrlByShort(shortUrl);
        if (url == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(LOCATION,url.getTarget());
            ResponseEntity<String> responseEntity = new ResponseEntity<>(httpHeaders,HttpStatus.FOUND);
            return responseEntity;
        }
    }
}
