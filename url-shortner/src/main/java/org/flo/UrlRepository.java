package org.flo;

import jdk.nashorn.internal.ir.WhileNode;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Component
public class UrlRepository {
    private Set<Url> urls = new TreeSet<>();

    public void addUrl(Url url) {
        urls.add(url);
    }

    public Set<Url> getUrls() {
        return urls;
    }

    public void addUrl(String target, Long lifeTime) {
        Url url = new Url();
        url.setTarget(target);
        url.setExpiresOn(LocalDateTime.now().plusMinutes(lifeTime));
        url.setShortUrl(generateRandomShortUrl());
        urls.add(url);
    }

    private String generateRandomShortUrl() {
        boolean shortUrlIsSet = false;
        String shortUrl;
        do {
            shortUrl = randomAlphanumeric(5);
            if (getUrlByShort(shortUrl) == null) {
                shortUrlIsSet = true;
            }
        } while (!shortUrlIsSet);
        return shortUrl;
    }

    public void deleteUrl(Url url) {
        urls.remove(url);
    }

    @Scheduled(cron="0 */2 * * * *")
    private void deleteExpiredUrls(){
        System.out.println("runned deleter at"+LocalDateTime.now());
        for (Url url: urls) {
            if(LocalDateTime.now().isAfter(url.getExpiresOn())){
             urls.remove(url);
            }
        }
    }
    public Url getUrlByShort(String shortUrl) {
        for (Url url : urls) {
            if (url.getShortUrl().equals(shortUrl)) {
                return url;
            }
        }
        return null;
    }


}
