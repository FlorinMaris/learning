package org.flo;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;

@Controller
public class UiController {
    private String hostName;
    @Autowired
    UrlRepository urlRepository;

    @PostConstruct
    private void getHostName() {

        File hostNameFile = new File("/etc/hostname");
        try {
            hostName=(Files.readAllLines(hostNameFile.toPath())).get(0);
        } catch (IOException e) {
            hostName="Could not find hostname file";
        }
    }

    @RequestMapping("/")
    public String welcome(Model model) {
        Set<Url> urls = urlRepository.getUrls();
        model.addAttribute("hostName", hostName);
        model.addAttribute("urls", urls);
        return "home";
    }

    @RequestMapping("/add_url")
    public String addUrl(@RequestParam(value = "urlTarget") String urlTarget,
                         @RequestParam(value = "lifeTime") Long lifeTime
    ) {
        urlRepository.addUrl(urlTarget, lifeTime);
        return "redirect:/";
    }


}