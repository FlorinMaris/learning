package org.flo;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class SystemEnvironmentVariableTest {
    @Test
    public void testMethod() {
        Map<String, String> env = System.getenv();
        System.out.println(env.get("USER" ));
         for (String envName : env.keySet()) {
            System.out.format("%s=%s%n",
                    envName,
                    env.get(envName));
        }

    }
}