# Url checker

A basic web app, that takes a target URL from a form and links it to a random alphanumerical
sequence which can be later used to redirect to the target URL.
The target URLs are valid for a selected period of time , after which they are deleted 
by a scheduled method 

A running version of the app can be viewed [here](https://akira.ddns.net/shtr/) 


The project will uses the following 

* [Maven](https://maven.apache.org/)
* [Spring boot](https://spring.io/guides/gs/serving-web-content/)
* [Tymeleaf](http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html)
* [Webjars](https://www.webjars.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)




